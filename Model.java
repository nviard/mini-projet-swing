import java.util.Observable;


public class Model extends Observable{
	private final static int DEFAULT_V = 0;
	private final static int DEFAULT_LB = -5;
	private final static int DEFAULT_UB = 25;
	
	private int value;
	private int upperBound;
	private int lowerBound;
	
	public Model(){
		this.value = DEFAULT_V;
		this.lowerBound = DEFAULT_LB;
		this.upperBound = DEFAULT_UB;
		this.setChanged();
	}
	
	public void incVal(){
		if(this.value < upperBound){
			this.value ++;
			this.setChanged();
			this.notifyObservers();
		}
	}
	
	public void decVal(){
		if(this.value > lowerBound){
			this.value --;
			this.setChanged();
			this.notifyObservers();
		}
	}
	
	public void setUpperBound(int newUpperBound){
		if(newUpperBound >= this.lowerBound){
			this.upperBound = newUpperBound;
			if(this.upperBound < this.value){
				this.value = this.upperBound;
			}
			this.setChanged();
			this.notifyObservers();
		}
	}
	
	public void setLowerBound(int newLowerBound){
		if(newLowerBound <= this.upperBound){
			this.lowerBound = newLowerBound;
			if(this.lowerBound > this.value){
				this.value = this.lowerBound;
			}
			this.setChanged();
			this.notifyObservers();
		}
	}

	public int getValue() {
		return this.value;
	}

	public int getUpperBound() {
		return this.upperBound;
	}

	public int getLowerBound() {
		return this.lowerBound;
	}
	
	
}
