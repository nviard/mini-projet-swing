public class Controller {
	private Model model;
	public Controller(Model m){
		this.model = m;
	}
	public void incValue(){
		this.model.incVal();
	}
	public void decValue(){
		this.model.decVal();
	}
	public void setUpperBound(String s){
		try{
			int bound = Integer.parseInt(s);
			this.model.setUpperBound(bound);
		} catch(NumberFormatException | NullPointerException e){
			System.err.println("Une borne non entiere a ete entree");
		};
	}
	public void setLowerBound(String s){
		try{
			int bound = Integer.parseInt(s);
			this.model.setLowerBound(bound);
		} catch(NumberFormatException | NullPointerException e){
			System.err.println("Une borne non entiere a ete entree");
		};
	}
}
