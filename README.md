Ce mini projet répond au sujet suivant :

Ecrire une application en Java, en utilisant la bibliothèque Swing.  

Cette application affiche la valeur (entière) d'un compteur qui ne peut prendre ses valeurs que dans un intervalle fixé (par exemple entre -5 et 25). Ce compteur peut être incrémenté/décrémenté dans les limites de l'intervalle, soit avec un bouton (+1 ou -1), soit avec un item du menu Modifier (+1 ou -1). Le menu Fichier inclut un seul item Quitter, permettant de quitter proprement l'application.

L'intervalle de variation de la valeur du compteur est fixé par défaut lors du lancement de l'application. Cet intervalle peut être modifié par les deux items du menu Intervalle. 

Sur la fenêtre principale de l'application, on doit voir la valeur du compteur, son intervalle de variation, les deux boutons et les deux menus. A vous de choisir la présentation de ces différents éléments. Vous pouvez vous appuyer sur les captures d'écrans fournies.

Il faut utiliser par exemple JLabel, JButton, JTextField, JFrame, JMenuBar, JMenu, JMenuItem, ActionListener, etc.