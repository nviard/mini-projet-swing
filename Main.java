
public class Main {

	public static void main(String[] args) {
		Model m = new Model();
		Controller c = new Controller(m);
		Window w = new Window(c);
		
		m.addObserver(w);
		m.notifyObservers();
		
	}

}
