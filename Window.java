import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;


public class Window extends JFrame implements Observer {

	private static final long serialVersionUID = 1L;

	private Controller controller;
		
	private JLabel lowerBound;
	private JLabel value;
	private JLabel upperBound;
	
	private ActionListener incL;
	private ActionListener decL;
	private ActionListener setLowerL;
	private ActionListener setUpperL;
	private ActionListener quitL;

	
	public Window(Controller c){
		super("Mini projet Swing");
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setPreferredSize(new Dimension(400, 400));
		this.setResizable(false);
		
		this.controller = c;
		
		this.incL = new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				Window.this.controller.incValue();
			}
		};
		
		this.decL = new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				Window.this.controller.decValue();
			}
		};
		
		this.setLowerL = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String s = JOptionPane.showInputDialog("Valeur minimale :");
				Window.this.controller.setLowerBound(s);
			}
		};
		
		this.setUpperL = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String s = JOptionPane.showInputDialog("Valeur maximale :");
				Window.this.controller.setLowerBound(s);
			}
		};
		
		this.quitL = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		};
		
		JMenuBar menuBar = new JMenuBar();
		this.add(menuBar, BorderLayout.NORTH);
		
		JMenu fichier = new JMenu("Fichier");
		menuBar.add(fichier);
			
			JMenuItem quitter = new JMenuItem("Quitter");
			quitter.addActionListener(this.quitL);
			fichier.add(quitter);
		
		JMenu modifier = new JMenu("Modifier");
		menuBar.add(modifier);
			JMenuItem dec = new JMenuItem("-1");
			dec.addActionListener(this.decL);
			modifier.add(dec);
			JMenuItem inc = new JMenuItem("+1");
			inc.addActionListener(this.incL);
			modifier.add(inc);
		
		JMenu intervalle = new JMenu("Intervalle");
		menuBar.add(intervalle);
			JMenuItem min = new JMenuItem("Definir la valeur minimale");
			min.addActionListener(this.setLowerL);
			intervalle.add(min);
			JMenuItem max = new JMenuItem("Definir la valeur maximale");
			max.addActionListener(this.setUpperL);
			intervalle.add(max);
	

		JPanel labels = new JPanel();
		this.add(labels, BorderLayout.CENTER);
		labels.setLayout(new GridLayout(1,3));
			this.lowerBound = new JLabel("0", SwingConstants.CENTER);
			labels.add(lowerBound);
			this.value = new JLabel("0", SwingConstants.CENTER);
			labels.add(value);
			this.upperBound = new JLabel("0", SwingConstants.CENTER);
			labels.add(upperBound);
		
		JPanel buttons = new JPanel();
		this.add(buttons, BorderLayout.SOUTH);
		labels.setLayout(new GridLayout(1,2));
			JButton decB = new JButton("-1");
			decB.addActionListener(this.decL);
			buttons.add(decB);
			JButton incB = new JButton("+1");
			incB.addActionListener(this.incL);
			buttons.add(incB);

		
		this.pack();
		this.setVisible(true);
	}
	
	public void update(Observable arg0, Object arg1) {
		Model m = (Model) arg0;
		this.lowerBound.setText("" + m.getLowerBound());
		this.value.setText("" + m.getValue());
		this.upperBound.setText("" + m.getUpperBound());

	}
	
}
